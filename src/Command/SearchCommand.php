<?php

namespace ElPaisBot\Command;

use Goutte\Client;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

/**
 * Class SearchCommand.
 */
final class SearchCommand extends Command
{
    private const SEARCH_URL_TEMPLATE = 'http://elpais.com/buscador/?qt=%s&sf=0&np=1&bu=ep&of=html';
    private const MAIN_HOST = 'http://elpais.com';
    const NUMBER_OF_SEARCH_RESULTS = 5;

    /**
     * {@inheritdoc}
     */
    protected $name = 'search';

    /**
     * {@inheritdoc}
     */
    protected $description = 'Search on El País.';

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {
        $this->replyWithChatAction(['action' => Actions::TYPING]);

        foreach ($this->getSearchResults($arguments) as $link) {
            $this->replyWithMessage(['text' => $link]);
        }
    }

    /**
     * @param string $topic
     *
     * @return string[]
     */
    private function getSearchResults($topic): array
    {
        $url = sprintf(self::SEARCH_URL_TEMPLATE, urlencode($topic));
        $browserEmulator = new Client();
        $this->obtainCookiesForSearch($browserEmulator, $url);
        $crawler = $browserEmulator->request('GET', $url);
        $linksToNews = $crawler->filter('h2 a')->extract('href');

        return array_map(function (string $path) {
            return self::MAIN_HOST.$path;
        }, array_slice($linksToNews, 0, self::NUMBER_OF_SEARCH_RESULTS));
    }

    /**
     * @param Client $browserEmulator
     * @param string $url
     */
    private function obtainCookiesForSearch(Client $browserEmulator, string $url): void
    {
        $browserEmulator->setClient(new \GuzzleHttp\Client(['cookie' => true]));
        $browserEmulator->request('GET', $url);
    }
}
