<?php

namespace ElPaisBot\Command;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Vinelab\Rss\Rss;

/**
 * Class LastCommand.
 */
final class LastCommand extends Command
{
    private const LAST_RSS_URL = 'http://ep00.epimg.net/rss/tags/ultimas_noticias.xml';

    private const DEFAULT_LAST_NEWS_NUMBER = 5;
    private const MAX_LAST_NEWS_NUMBER = 10;

    /**
     * {@inheritdoc}
     */
    public $name = 'last';

    /**
     * {@inheritdoc}
     */
    protected $description = 'Get last 5 news on País.';

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {
        $this->replyWithChatAction(['action' => Actions::TYPING]);

        $rssClient = new Rss();
        $feed = $rssClient->feed(self::LAST_RSS_URL);

        $articles = $feed->articles->take($this->getNumberOfNewsToSend($arguments));

        foreach ($articles as $article) {
            $this->replyWithMessage(['text' => $article->link]);
        }
    }

    /**
     * @param mixed $arguments
     *
     * @return int
     */
    private function getNumberOfNewsToSend($arguments): int
    {
        $requestedLastNewsNumber = abs((int) $arguments);
        if (!$arguments) {
            return self::DEFAULT_LAST_NEWS_NUMBER;
        }

        if ($requestedLastNewsNumber > self::MAX_LAST_NEWS_NUMBER) {
            return self::MAX_LAST_NEWS_NUMBER;
        }

        return $requestedLastNewsNumber;
    }
}
