<?php

namespace ElPaisBot\Command;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Vinelab\Rss\Rss;

/**
 * Class TopCommand.
 */
final class TopCommand extends Command
{
    private const TOP_RSS_URL = 'http://ep00.epimg.net/rss/tags/noticias_mas_vistas.xml';
    private const TOP_NEWS_NUMBER = 3;

    /**
     * {@inheritdoc}
     */
    public $name = 'top';

    /**
     * {@inheritdoc}
     */
    protected $description = 'Search on El País.';

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {
        $this->replyWithChatAction(['action' => Actions::TYPING]);

        $rssClient = new Rss();
        $feed = $rssClient->feed(self::TOP_RSS_URL);

        $articles = $feed->articles->take(self::TOP_NEWS_NUMBER);

        foreach ($articles as $article) {
            $this->replyWithMessage(['text' => $article->link]);
        }
    }
}
