<?php

namespace ElPaisBot\Command;

use ElPaisBot\App\Botan;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

/**
 * Class StartCommand.
 */
final class StartCommand extends Command
{
    private const METRICS_EVENT = 'Start';
    /**
     * @var string
     */
    protected $name = 'start';

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {
        $this->replyWithMessage(['text' => 'Hello! Here\'s what I can do:']);
        $this->replyWithChatAction(['action' => Actions::TYPING]);

        $commands = $this->getTelegram()->getCommands();

        $this->replyWithMessage(['text' => $this->getCommandsList($commands)]);
        $this->noticeEvent();
    }

    /**
     * @param Command[] $commands
     *
     * @return string
     */
    private function getCommandsList(array $commands): string
    {
        $response = '';
        foreach ($commands as $name => $command) {
            if ($command->getDescription()) {
                $response .= sprintf('/%s - %s'.PHP_EOL, $name, $command->getDescription());
            }
        }

        return $response;
    }

    private function noticeEvent(): void
    {
        $botanToken = getenv('BOTAN_TOKEN');
        if ($botanToken) {
            $botan = new Botan($botanToken);
            $botan->track($this->getUpdate()->getMessage(), self::METRICS_EVENT);
        }
    }
}
