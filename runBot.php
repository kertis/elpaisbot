<?php

require_once './vendor/autoload.php';

$telegram = new \Telegram\Bot\Api(getenv('TELEGRAM_TOKEN'));

$telegram->addCommands([
    \ElPaisBot\Command\StartCommand::class,
    \ElPaisBot\Command\TopCommand::class,
    \ElPaisBot\Command\LastCommand::class,
    \ElPaisBot\Command\SearchCommand::class,
]);

/**
 * @param Exception $exception
 */
function logError(\Exception $exception): void
{
    echo sprintf(
        'Error %s - %s in %s on %d when processing %s',
        $exception->getMessage(),
        $exception->getCode(),
        $exception->getFile(),
        $exception->getLine()
    );
}

while (true) {
    try {
        $update = $telegram->commandsHandler();
    } catch (\Exception $e) {
        logError($e);
    }
}
