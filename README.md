# ElPaisBot #

This is a telegram bot for http://elpais.com

### How do I get set up? ###

* Install [Docker](https://www.docker.com)
* [Create a bot](https://core.telegram.org/bots#3-how-do-i-create-a-bot) 
* Get Botan/Appmetrica token as described [here](https://github.com/botanio/sdk#botan-sdk)
* Run a container:
```
#!bash

docker run -e TELEGRAM_TOKEN=<YOUR_TELEGRAM_TOKEN> -e BOTAN_TOKEN=<YOUR_BOTAN_TOKEN> --name=elpaisbot --restart=always -d kertis/elpaisbot
```


### Related links ###

* [Telegram Bot SDK](https://telegram-bot-sdk.readme.io/docs)
* [Analytics for bots](http://botan.io)